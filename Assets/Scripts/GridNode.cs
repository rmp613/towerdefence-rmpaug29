﻿using UnityEngine;
using System.Collections;

public class GridNode {
	public bool walkable;
	public Vector3 worldPos;
	public float moveCost;

	public GridNode(bool walkable, Vector3 worldPos, float moveCost){
		this.walkable = walkable;
		this.worldPos = worldPos;
		this.moveCost = moveCost;
	}
	public GridNode(bool walkable, Vector3 worldPos){
		this.walkable = walkable;
		this.worldPos = worldPos;
	}
}
